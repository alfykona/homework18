﻿#include <iostream>
#include <vector>
#include <string>
using namespace std;
template<typename T>
class Stack
{
public:
    void push(T);
    T pop();
    void show();
private:
    vector<T> v;
};

int main() {
    
    Stack<string> a;
    a.push("hello"); a.push("Skillbox");
    a.show();
    cout << "pop: " << a.pop() << "\n";
    a.show();
    
    
    Stack<double> b;
    b.push(1.1);  b.push(2.2);  b.push(3.3);
    b.show();
    cout << "pop: " << b.pop() << "\n";
    b.show();


    return 0;
}

template<class T> void Stack<T>::push(T element)
{
    v.push_back(element);
}

template<class T> T Stack<T>::pop()
{
    T element = v.back();
    v.pop_back();
    return element;
}
template<class T> void Stack<T>::show()
{
    cout << "stack: ";
    for (auto e : v) cout << e << " ";
    cout << "\n";
}
